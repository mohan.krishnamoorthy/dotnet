# .NET image version 6.2
FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build-env

# Installing Git
RUN apt-get update && apt-get install -y git

# Installing Maven
RUN apt-get install -y maven

# Installing SonarScanner
RUN apt-get install -y unzip
RUN apt-get install -y wget
RUN wget https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-4.6.0.2311-linux.zip
RUN unzip sonar-scanner-cli-4.6.0.2311-linux.zip -d /opt
RUN rm sonar-scanner-cli-4.6.0.2311-linux.zip
ENV PATH="$PATH:/opt/sonar-scanner-4.6.0.2311-linux/bin"

# Set the working directory in the container
WORKDIR /app

# Copy the project files into the container
COPY . .

# Build the .NET Core application
RUN dotnet build -c Release

# Expose the port
EXPOSE 80

# Run the application
CMD ["dotnet", "run", "--urls", "http://0.0.0.0:80"]
